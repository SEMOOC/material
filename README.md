Based on, but not afiliated with, the [Systems Engineering MOOC](https://www.coursera.org/learn/systems-engineering)

related standards: EIA-632, ISO/IEC/IEEE 29148, SEBoK

Please, take a look at the  [repository](https://gitlab.com/SEMOOC/material/tree/master) and the document ["contribution guide"](https://gitlab.com/SEMOOC/material/blob/master/CONTRIBUTING.md)


*\*.puml* files are the diagrams' source code which in turn gets compiled to get the *\*.png* and *\*.svg* versions
