## How to contribute

This repository relies mainly on the use of [git](http://www.git.org), for uploading modifications to the [PlantUML](http://plantuml.com) (\*.puml) files.  
[PlantUML](http://plantuml.com) is the purposed language  for the diagrams' *source code* which in turn gets compiled to get the \*.png and \*.svg versions.
Other ways to contribute is are:

* raising [issues](https://gitlab.com/SEMOOC/material/wikis/home)
* elaborating [wikis](https://gitlab.com/SEMOOC/material/issues)


